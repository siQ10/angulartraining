import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
    @Input() imgsource: String;
    @Input() description: String;
    @Input() color: String;
    @Input() order?: Number;

  constructor() { 
    
  }

  ngOnInit() {
  }

}
