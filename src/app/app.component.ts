import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Learning Angular';
  sections : Array<Object>;
  links:Array<String>;
  logo:String;

constructor(){
  this.sections = [
  {
    "image":"https://cdn-images-1.medium.com/max/588/1*15CYVZdpsxir8KLdxEZytg.png",
    
    "description":"AngularJS represents a structural framework for dynamic web apps. It lets you use HTML as your template language and lets you extend HTML's syntax to express your application's components clearly and succinctly. AngularJS's data binding and dependency injection eliminate much of the code you would otherwise have to write.",
      "color":'red',
      "order":0
  },
  {
  "image":"https://cdn-images-1.medium.com/max/1600/1*ieROYZuCX-w0p9V7UswJbQ.png",
  "description":"Vue is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects.",
      "color":'darkgreen',
      "order":1


  },

  {
  "image":"https://react-etc.net/files/2017-12/react-hexagon.png",
  "description":"React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes. Declarative views make your code more predictable and easier to debug.",
  "color":"teal",
      "order":2

  },
  
  ]
  this.links = [
      "Home",
      "Tutorials",
      "APIs",
      "About",
  ]
    
}
}
