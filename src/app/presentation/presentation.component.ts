import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.css']
})
export class PresentationComponent implements OnInit {
    @Input() sections: Array<any>;

  constructor() { 
      console.log(this.sections)
  }
  
  goLeft(){
      this.sections.forEach(section => {
        section.order == 0 ? section.order = 2 : section.order = (section.order-1) % 3
      })
      this.sections = this.sections.sort((a,b)=>{
         return a.order > b.order ? 1 : -1
      })
                 this.sections.forEach(section=> {console.log(section.order)})
      console.log(this.sections)

  }
  goRight(){
        this.sections.forEach(section => {
        section.order == 3 ? section.order = 0 : section.order = (section.order+1) % 3
      })
      this.sections = this.sections.sort((a,b)=>{
         return a.order > b.order ? 1 : -1
      })
            this.sections.forEach(section=> {console.log(section.order)})
      console.log(this.sections)

 }
  ngOnInit() {

  }

}
