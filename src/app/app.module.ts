import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PresentationComponent } from './presentation/presentation.component';
import { SectionComponent } from './section/section.component';
import { NavbarComponent } from './navbar/navbar.component';

//const appRoutes: Routes = [
//  { path: "", component: CrisisListComponent },
//  { path: 'hero/:id',      component: HeroDetailComponent },
//  {
//    path: 'heroes',
//    component: HeroListComponent,
//    data: { title: 'Heroes List' }
//  },
//  { path: '',
//    redirectTo: '/heroes',
//    pathMatch: 'full'
//  },
//  { path: '**', component: PageNotFoundComponent }
//];

@NgModule({
  declarations: [
    AppComponent,
    PresentationComponent,
    SectionComponent,
    NavbarComponent
  ],
  imports: [
//    RouterModule.forRoot(
//      appRoutes,
//      { enableTracing: true } // <-- debugging purposes only
//    )
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
